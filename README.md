# 2D Collective Communication For The Cerebras Wafer-Scale Engine
Repo with experiments for collective communication on the WSE-2.


## How to run the experiments
Experiments are provided for the following Communication Patterns.
 - Gather
 - AllGather
   - Naive
   - DoubleStar
   - Ring
 - Reduce-Scatter
 - Reduce
 - AllReduce

Experiments for individual patterns can be run by typing `./**Collective_Name**_commands.sh`.

