
param memcpy_params: comptime_struct;
param B : u16;  // Vector-Size
param Pw : u16; // #PEs in x dir

// Algos:
// 0 -> simple
param algo : u16;

param color_1 : color;
param color_2 : color;
param color_3 : color;
param color_4 : color;
param color_5 : color;
param color_6 : color;
param color_7 : color;
param color_8 : color;
param color_9 : color;

param local_task_id_1 : local_task_id;
param local_task_id_2 : local_task_id;
param local_task_id_3 : local_task_id;
param local_task_id_4 : local_task_id;
param local_task_id_5 : local_task_id;
param local_task_id_6 : local_task_id;
param local_task_id_7 : local_task_id;
param local_task_id_8 : local_task_id;
param exit_task_id : local_task_id;
param bcast_right_id: local_task_id;
param finish_sync_id: local_task_id;

const sys_mod = @import_module("<memcpy/memcpy>", memcpy_params);
const timestamp = @import_module("<time>");
const math = @import_module("<math>");
const layout_mod = @import_module("<layout>");

var pe_id_x : u16;
var pe_id_y : u16;

const reduce_2d = @import_module("external/2d_chain_runtime.csl", .{.color_1 = color_5, .color_2 = color_6, .M = Pw, .N = Pw, .timestamp = timestamp});
const broadcast_2d = @import_module("external/2d_broadcast_runtime.csl", .{.broadcast_color = color_7, .M = Pw, .N = Pw, .timestamp = timestamp, .root_x = 0, .root_y = 0});


const rs_2d = @import_module("modules/2D_scatter_reduce.csl", .{.north_color = color_1, .south_color = color_2, .east_color = color_3, .west_color = color_4,
                                                    .Pw = Pw,
                                                    .send_task_id = local_task_id_1, .send_ctrl_task_id = local_task_id_2, .send_tear_task_id = local_task_id_3,
                                                    .next_round_task_id = local_task_id_4, .done_receiving_task_id = local_task_id_5,
                                                    .horizontal_teardown_task_id = local_task_id_6, .vertical_teardown_task_id = local_task_id_7,
                                                    .exit_task_id = local_task_id_8,
                                                    .timestamp = timestamp});


const gather_2d = @import_module("modules/2D_gather_improved.csl", .{.green = color_8, .blue = color_9, .Pw=Pw, .Ph=Pw, .timestamp = timestamp});


var tscStartBuffer = @zeros([timestamp.tsc_size_words]u16);
var tscEndBuffer = @zeros([timestamp.tsc_size_words]u16);
var tscRefBuffer = @zeros([timestamp.tsc_size_words]u16);
var tscDummyBuffer = @zeros([timestamp.tsc_size_words]u16);


var time_buf_u16 = @zeros([timestamp.tsc_size_words*2]u16);
var time_ref_u16 = @zeros([timestamp.tsc_size_words]u16);

var ptr_time_buf_u16: [*]u16 = &time_buf_u16;
var ptr_time_ref: [*]u16 = &time_ref_u16;

var x = @constants([4096]f32, @as(f32, 1.0));
var x_ptr: [*]f32 = &x;

var dummy = @zeros([2048]f32);
const dummy_mem_dsd = @get_dsd(mem1d_dsd, .{ .tensor_access = |i|{2048} -> dummy[i]});
var xd = @zeros([@as(u16, 2.0*@as(f32, (Pw + Pw)))]f32);
var xd_mem_dsd = @get_dsd(mem1d_dsd, .{ .tensor_access = |i|{1} -> xd[i]});
var warmup_iter: u16 = 0;

fn f_sync(n: i16) void {
    timestamp.enable_tsc();

    pe_id_x = layout_mod.get_x_coord();
    pe_id_y = layout_mod.get_y_coord();
    xd_mem_dsd = @set_dsd_length(xd_mem_dsd, @as(u16, 2.0*@as(f32, (Pw + Pw - pe_id_y - pe_id_x))));

    reduce_2d.setup();
    broadcast_2d.setup();
    rs_2d.configure_network(pe_id_x, pe_id_y);
    gather_2d.configure_network(pe_id_x, pe_id_y);

    @fadds(dummy_mem_dsd, dummy_mem_dsd, 1.0);
    @fadds(dummy_mem_dsd, dummy_mem_dsd, 1.0);
    reduce_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscDummyBuffer);
}

task bcast_right() void {
  if (warmup_iter == 0) {
    warmup_iter += 1;
    broadcast_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscDummyBuffer);
  } else if (warmup_iter == 1) {
    warmup_iter += 1;
    broadcast_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscDummyBuffer);
  } else if (warmup_iter == 2) {
    warmup_iter += 1;
    reduce_2d.transfer_data(&dummy, 2048, bcast_right_id, &tscDummyBuffer);
  } else if (warmup_iter == 3) {
    broadcast_2d.transfer_data(&dummy, 2048, finish_sync_id, &tscDummyBuffer);
  }
  
}

task finish_sync() void {
  timestamp.get_timestamp(&tscRefBuffer);
  delay();
  compute();
}

// Function to delay the start of compute by some number of cycles in order for all PEs to start at a relatively similar time
fn delay() void {
  @fmovs(xd_mem_dsd, 1.0);
}


fn compute() void {
  rs_2d.transfer_data(x_ptr, x_ptr, B, exit_task_id, &tscStartBuffer, &tscDummyBuffer);
}

var k: u16 = 0;
task exit_task() void {
    if (k == 0){
      k = 1;
      gather_2d.transfer_data(&dummy, x_ptr, comptime B/(Pw*Pw), exit_task_id, &tscDummyBuffer, &tscEndBuffer);
      return;
    }

    sys_mod.unblock_cmd_stream();
}

fn f_memcpy_timestamps() void {

  time_buf_u16[0] = tscStartBuffer[0];
  time_buf_u16[1] = tscStartBuffer[1];
  time_buf_u16[2] = tscStartBuffer[2];

  time_buf_u16[3] = tscEndBuffer[0];
  time_buf_u16[4] = tscEndBuffer[1];
  time_buf_u16[5] = tscEndBuffer[2];

  time_ref_u16[0] = tscRefBuffer[0];
  time_ref_u16[1] = tscRefBuffer[1];
  time_ref_u16[2] = tscRefBuffer[2];

  sys_mod.unblock_cmd_stream();
}

comptime{
    @bind_local_task(exit_task, exit_task_id);
    @bind_local_task(bcast_right, bcast_right_id);
    @bind_local_task(finish_sync, finish_sync_id);

    @export_symbol(x_ptr, "x");
    @export_symbol(ptr_time_buf_u16, "time_buf_u16");
    @export_symbol(ptr_time_ref, "time_ref");

    @export_symbol(compute);
    @export_symbol(f_sync);
    @export_symbol(f_memcpy_timestamps);

}