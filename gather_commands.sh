#!/usr/bin/env bash
set -e

export SINGULARITYENV_CSL_SUPPRESS_SIMFAB_TRACE=1
# export SINGULARITYENV_SIMFABRIC_DEBUG=landing

for x_pow in {1..6}
do

    max_vec_len=$((13-2*x_pow))
    for (( vec_pow=1; vec_pow<=max_vec_len; vec_pow++ ))
    do
        num_pes_x=$((2**$x_pow))
        vec_size=$((2**$vec_pow))

        x_dim=$((7+$num_pes_x))
        y_dim=$((2+$num_pes_x))

        sqrt_result=$(echo "sqrt(512)" | bc -l)
        int_result=${sqrt_result%.*}
        cslc gather_layout.csl --fabric-dims=$x_dim,$y_dim \
        --fabric-offsets=4,1 --params=Pw:$num_pes_x,Ph:$num_pes_x,B:$vec_size -o out --memcpy --channels=1
        cs_python gather_run.py --name out
        # cp data_2d.csv $HOME/git_ethz/cerebras/Modular_Implementations/results/data_2d_gather_P.txt
        # cp results_2d.txt $HOME/git_ethz/cerebras/Modular_Implementations/results/results_2d_gather_P.txt
    done
done